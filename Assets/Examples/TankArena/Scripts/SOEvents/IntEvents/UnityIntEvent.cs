using System;
using UnityEngine.Events;

namespace Examples.TankArena.Scripts.SOEvents.IntEvents {
    [Serializable] public class UnityIntEvent : UnityEvent<int> {}
}