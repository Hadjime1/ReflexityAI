using UnityEngine;

namespace Examples.TankArena.Scripts.SOEvents.IntEvents {
    [CreateAssetMenu(fileName = "Int_OnEvent", menuName = "SOEvent/Int")]
    public class IntEvent : BaseGameEvent<int> {}
}