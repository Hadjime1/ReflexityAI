using UnityEngine;

namespace Examples.TankArena.Scripts.SOEvents.GameObjectEvents {

    public class GameObjectEventListener : BaseGameEventListener<GameObject, GameObjectEvent, UnityGameObjectEvent> { }
}