using System;
using UnityEngine.Events;

namespace Examples.TankArena.Scripts.SOEvents.BoolEvents {
    [Serializable] public class UnityBoolEvent : UnityEvent<bool> {}
}