﻿using UnityEngine;

namespace Examples.TankArena.Scripts.SOEvents.BoolEvents {
    [CreateAssetMenu(fileName = "Bool_OnEvent", menuName = "SOEvent/Bool")]
    public class BoolEvent : BaseGameEvent<bool> {}

}
