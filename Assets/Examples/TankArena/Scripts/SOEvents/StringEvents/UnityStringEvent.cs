using System;
using UnityEngine.Events;

namespace Examples.TankArena.Scripts.SOEvents.StringEvents {
    [Serializable] public class UnityStringEvent : UnityEvent<string> {}
}