﻿using UnityEngine;

namespace Examples.TankArena.Scripts.SOEvents.StringEvents {
    [CreateAssetMenu(fileName = "String_OnEvent", menuName = "SOEvent/String")]
    public class StringEvent : BaseGameEvent<string> {}

}
