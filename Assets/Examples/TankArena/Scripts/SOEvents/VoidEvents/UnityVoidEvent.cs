using System;
using UnityEngine.Events;

namespace Examples.TankArena.Scripts.SOEvents.VoidEvents {
    [Serializable] public class UnityVoidEvent : UnityEvent<Void> {}
}