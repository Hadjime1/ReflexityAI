using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.IntReference {
    [CreateAssetMenu(fileName = "Int_Variable", menuName = "SOVariable/Int")]
    public class IntVariable : Variable<int> {}
}