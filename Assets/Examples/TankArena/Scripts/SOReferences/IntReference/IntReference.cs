﻿using System;

namespace Examples.TankArena.Scripts.SOReferences.IntReference {
    [Serializable]
    public class IntReference : Reference<int, IntVariable> {}
}