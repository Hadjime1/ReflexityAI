﻿using System;
using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.RendererReference {
    [Serializable]
    public class RendererReference : Reference<Renderer, RendererVariable> {}
}
