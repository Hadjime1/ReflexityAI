using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.RendererReference {
    [CreateAssetMenu(fileName = "Renderer_Variable", menuName = "SOVariable/Renderer")]
    public class RendererVariable : Variable<Renderer> { }
}