using System;
using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.Vector3Reference {
    [Serializable]
    public class Vector3Reference : Reference<Vector3, Vector3Variable> {}
}