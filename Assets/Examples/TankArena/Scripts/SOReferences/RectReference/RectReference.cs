using System;
using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.RectReference {
    [Serializable]
    public class RectReference : Reference<Rect, RectVariable> {}
}