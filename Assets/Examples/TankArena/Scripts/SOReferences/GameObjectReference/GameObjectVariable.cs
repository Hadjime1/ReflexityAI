﻿using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.GameObjectReference {

    [CreateAssetMenu(fileName = "GameObject_Variable", menuName = "SOVariable/GameObject")]
    public class GameObjectVariable : Variable<GameObject> { }
}