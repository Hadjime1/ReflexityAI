using System;
using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.GameObjectReference {
    [Serializable]
    public class GameObjectReference : Reference<GameObject, GameObjectVariable> {}
}