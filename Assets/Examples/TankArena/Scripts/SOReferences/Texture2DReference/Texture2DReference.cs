using System;
using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.Texture2DReference {
    [Serializable]
    public class Texture2DReference : Reference<Texture2D, Texture2DVariable> {}
}