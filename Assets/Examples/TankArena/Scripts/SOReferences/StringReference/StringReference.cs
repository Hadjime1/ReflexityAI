using System;

namespace Examples.TankArena.Scripts.SOReferences.StringReference {
    [Serializable]
    public class StringReference : Reference<string, StringVariable> {}
}