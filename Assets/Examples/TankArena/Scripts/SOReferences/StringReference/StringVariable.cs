using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.StringReference {

    [CreateAssetMenu(fileName = "String_Variable", menuName = "SOVariable/String")]
    public class StringVariable : Variable<string> { }
}