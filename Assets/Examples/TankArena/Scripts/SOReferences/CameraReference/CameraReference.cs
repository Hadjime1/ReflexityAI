using System;
using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.CameraReference {
    [Serializable]
    public class CameraReference : Reference<Camera, CameraVariable> {}
}