using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.CameraReference {
    [CreateAssetMenu(fileName = "Camera_Variable", menuName = "SOVariable/Camera")]
    public class CameraVariable : Variable<Camera> {}
}