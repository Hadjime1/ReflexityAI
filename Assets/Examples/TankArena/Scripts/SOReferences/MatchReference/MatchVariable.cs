using Examples.TankArena.Scripts.Framework;
using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.MatchReference {
    [CreateAssetMenu(fileName = "Match_Variable", menuName = "SOVariable/Match")]
    public class MatchVariable : Variable<Match> {}
}