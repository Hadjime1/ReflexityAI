using System;
using Examples.TankArena.Scripts.Framework;

namespace Examples.TankArena.Scripts.SOReferences.MatchReference {
    [Serializable]
    public class MatchReference : Reference<Match, MatchVariable> {}
}