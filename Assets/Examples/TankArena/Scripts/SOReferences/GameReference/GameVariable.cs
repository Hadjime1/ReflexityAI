using Examples.TankArena.Scripts.Framework;
using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.GameReference {
    [CreateAssetMenu(fileName = "Tournament_Variable", menuName = "SOVariable/Tournament")]
    public class GameVariable : Variable<Game> {}
}