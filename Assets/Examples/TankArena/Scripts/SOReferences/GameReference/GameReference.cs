﻿using System;
using Examples.TankArena.Scripts.Framework;

namespace Examples.TankArena.Scripts.SOReferences.GameReference {
    [Serializable]
    public class GameReference : Reference<Game, GameVariable> {}
}
