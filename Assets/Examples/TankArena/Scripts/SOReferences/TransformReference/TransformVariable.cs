using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.TransformReference {
    [CreateAssetMenu(fileName = "Transform_Variable", menuName = "SOVariable/Transform")]
    public class TransformVariable : Variable<Transform> { }
}