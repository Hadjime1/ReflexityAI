﻿using System;
using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.TransformReference {
    [Serializable]
    public class TransformReference : Reference<Transform, TransformVariable> {}

}
