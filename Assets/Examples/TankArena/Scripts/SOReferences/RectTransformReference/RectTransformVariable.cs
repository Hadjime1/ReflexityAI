﻿using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.RectTransformReference {

    [CreateAssetMenu(fileName = "RectTransform_Variable", menuName = "SOVariable/RectTransform")]
    public class RectTransformVariable : Variable<RectTransform> { }
}