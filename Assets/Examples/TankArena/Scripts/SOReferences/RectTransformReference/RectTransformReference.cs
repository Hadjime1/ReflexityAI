using System;
using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.RectTransformReference {
    [Serializable]
    public class RectTransformReference : Reference<RectTransform, RectTransformVariable> {}
}