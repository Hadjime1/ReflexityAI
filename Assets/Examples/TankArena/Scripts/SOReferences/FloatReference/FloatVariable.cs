using UnityEngine;

namespace Examples.TankArena.Scripts.SOReferences.FloatReference {
    [CreateAssetMenu(fileName = "Float_Variable", menuName = "SOVariable/Float")]
    public class FloatVariable : Variable<float> {}
}