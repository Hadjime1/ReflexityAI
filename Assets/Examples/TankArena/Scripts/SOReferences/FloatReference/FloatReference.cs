﻿using System;

namespace Examples.TankArena.Scripts.SOReferences.FloatReference {
    [Serializable]
    public class FloatReference : Reference<float, FloatVariable> {}
}
