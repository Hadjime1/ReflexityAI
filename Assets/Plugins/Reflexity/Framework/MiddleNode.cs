﻿using XNode;

namespace Plugins.Reflexity.Framework {
    [NodeTint(31, 54, 122)]
    public abstract class MiddleNode : Node {}
}