namespace Plugins.Reflexity.Framework {
    public interface IContextual {
        
        ReflexityAI Context { get; set; }
        
    }
}