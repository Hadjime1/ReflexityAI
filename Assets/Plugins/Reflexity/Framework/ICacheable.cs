namespace Plugins.Reflexity.Framework {
    public interface ICacheable {

        void ClearCache();
        void ClearShortCache();

    }
}